const SimpleStorage = artifacts.require('SimpleStorage.sol');
const SimpleTests = artifacts.require('SimpleTests.sol');
const BasicNFT = artifacts.require('BasicNFT.sol');

module.exports = async function (deployer, _network, accounts) {
	await deployer.deploy(SimpleStorage);
	await deployer.deploy(SimpleTests);
	
	await deployer.deploy(BasicNFT);
	// console.log(accounts[0]);
	const nft = await BasicNFT.deployed();
	await nft.mint(accounts[0]);
	await nft.mint(accounts[0]);
	await nft.mint(accounts[0]);
	await nft.mint(accounts[0]);
	await nft.mint(accounts[0]);
};

