pragma solidity 0.8.9;

import '@openzeppelin/contracts/token/ERC721/ERC721.sol';

contract BasicNFT is ERC721 {
	uint public nextTokenId;
	address public admin;

	constructor() ERC721('Basic NFT', 'BASICNFT') {
		admin = msg.sender;
	}

	function mint(address to) external {
		require(msg.sender == admin, 'only admin');
		_safeMint(to, nextTokenId);
		nextTokenId++;
	}

	function _baseURI() internal view override returns(string memory) {
		return 'http://localhost:3000/examples/basicnft/';
	}

}