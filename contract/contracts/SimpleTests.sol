pragma solidity 0.8.9;

contract SimpleTests {
	uint currentNumber = 1;
	string baseUrl = "https://www.example.com/";

	function setCurrentNumber(uint _currentNumber) external {
		currentNumber = _currentNumber;
	}

	function getCurrentNumber() external view returns(uint) {
		return currentNumber;
	}

	function getUrl() external view returns(string memory) {
		string memory appendStr = "hello";
		return string(abi.encodePacked(baseUrl, appendStr));
	}

}