# Clean Example of an NFT project as a starter (while learning)

Includes basic steps on how it was setup/created


## React Frontend Setup

Usage:

Ensure using Node v12!

`npm start`


Uses:
* Node v12.22.5
* Tailwind

Setup With:
* `npx create-react-app frontend`
* `cd frontend`
* `npm install -D tailwindcss@npm:@tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9`
* `npm install @craco/craco`


## Contract 

SimpleStorage is just a barebones contract to Set and Get one value on the contract.

By default now uses the default network (which is 'development') so all commands below will interact with that by default. Warning,
`truffle develop` interacts with it's own separate blockchain.

Usage:

Compile contracts
	`truffle compile`

Test:
	`truffle test`

Deploy:
	`truffle migrate --reset`

Testing/Interaction on CLI:
	`truffle console`			(connects to default network which should be development)
	$ `let instance = await SimpleStorage.deployed()`
	$ `instance.address` (view address of contract)
	$ `await instance.updateData(10)`	Use function to set value

	Read the data
	$ `data = await instance.readData()`
	$ `data.toString()`


More examples:
	`data = await instance.getUrl()`
	`data.toString()`



Main Ref:
	https://www.youtube.com/watch?v=62f757RVEvU&t=197s
Also:
	https://www.youtube.com/watch?v=nU0uqk0jLdw
	https://trufflesuite.com/boxes/react

Uses:
* Truffle

Setup With:
* `truffle init`


### Contract Specifics/Info

#### SimpleStorage

Stores and retrieves a number


#### SimpleTests

Simple Testing/Examples of a variety of functions for various future uses, eg concatenation of strings, etc


#### Basic NFT

Refs:	https://www.youtube.com/watch?v=WsZyb2T83lo&t=492s (contract & some frontend)


Interact:
	`truffle console` then:
		`nft = await BasicNFT.deployed()`
		`await nft.name()`
		`await nft.symbol()`
		`await nft.mint('<ADDRESS>')`







Useful info

* Using Ganache GUI with Truffle: https://ethereum.stackexchange.com/questions/32825/how-to-configure-truffle-to-use-the-ganache-gui-instead-of-ganache-cli-testrpc


