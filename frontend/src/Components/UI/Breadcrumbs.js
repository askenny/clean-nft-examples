import { HomeIcon } from '@heroicons/react/solid'


export default function Breadcrumbs({ pages }) {

	// eg:
	// const pages = [
	// 	{ name: 'Contracts', href: '#'},
	// 	{ name: 'Simple Contract', href: '#'},
	// ]


	return (
		<div className="p-5 bg-white border-b border-gray-200 flex">
			<div className="container mx-auto">
				<nav className="flex" aria-label="Breadcrumb">
					<ol className="flex items-center space-x-4">
						<li>
							<div>
								<a href="/" className="text-gray-400 hover:text-gray-500">
									<HomeIcon className="flex-shrink-0 h-5 w-5" aria-hidden="true" />
									<span className="sr-only">Home</span>
								</a>
							</div>
						</li>
						{pages && pages.map((page) => (
							<li key={page.name}>
								<div className="flex items-center">
									<svg className="flex-shrink-0 h-5 w-5 text-gray-300" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20" aria-hidden="true"><path d="M5.555 17.776l8-16 .894.448-8 16-.894-.448z" /></svg>
									<a href={page.href} className="ml-2 text-sm font-medium text-gray-500 hover:text-gray-700">
										{page.name}
									</a>
								</div>
							</li>
						))}
					</ol>
				</nav>
			</div>
		</div>

	)
}
