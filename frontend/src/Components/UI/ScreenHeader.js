
const ScreenHeader = ({ title, description}) => (
    <div className="p-5 bg-white border-b border-gray-200 flex">
		<div className="container mx-auto">
			<div className="pb-3  max-w-2xl">
				<h2 className="text-2xl leading-8 font-bold text-gray-900">{title}</h2>
				<p className="mt-2 text-md text-gray-500">
					{description}
			  </p>
			</div>
		</div>
	</div>
);

export default ScreenHeader;
