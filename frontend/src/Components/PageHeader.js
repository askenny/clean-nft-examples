import { BriefcaseIcon } from '@heroicons/react/outline'

const PageHeader = () => (
	<div className="relative  py-10 overflow-hidden bg-gray-800">
		<div aria-hidden="true" className="absolute inset-0 -mt-72 sm:-mt-32 md:mt-0">
			<svg className="absolute inset-0 h-full w-full" preserveAspectRatio="xMidYMid slice" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 1463 360">
				<path className="" fill="#293344" d="M-82.673 72l1761.849 472.086-134.327 501.315-1761.85-472.086z" />
				<path className="" fill="#323F54" d="M-217.088 544.086L1544.761 72l134.327 501.316-1761.849 472.086z" />
			</svg>
		</div>
		<div className="relative">

				<div className="container mx-auto ">
					<div className="mt-2 md:flex md:items-center md:justify-between">
						<div className="flex-1 min-w-0">
							<h2 className="text-2xl font-bold leading-7 text-white sm:text-3xl sm:truncate"> <BriefcaseIcon className="inline h-9 w-9 -mt-2" aria-hidden="true" /> Smart Contract Test Suite</h2>
						</div>
						<div className="mt-4 flex-shrink-0 flex md:mt-0 md:ml-4">
							{/*
							<button type="button" className="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-indigo-500">Edit</button>
							<button type="button" className="ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-indigo-500">Publish</button>
							 */}
						</div>
					</div>
				</div>

		</div>
	</div>
);

export default PageHeader;
