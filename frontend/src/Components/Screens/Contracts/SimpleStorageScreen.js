import React, { Component } from "react";
import PageHeader from '../../PageHeader';
import Breadcrumbs from '../../UI/Breadcrumbs';
import ScreenHeader from '../../UI/ScreenHeader';
import SimpleStorageContract from '../../../contracts/SimpleStorage.json';
import getWeb3 from "../../../lib/getWeb3";

class SimpleStorageScreen extends Component {
	state = {
		storageValue: 0, 
		web3: null,
		accounts: null,
		contract: null,
		formVal: '',
	};

	componentDidMount = async () => {
		try {
			const web3 = await getWeb3();
			const accounts = await web3.eth.getAccounts();
			const networkId = await web3.eth.net.getId();
			// console.log("Network ID", networkId);
			// const chainId = await web3.eth.getChainId();
			// console.log("Chain ID", chainId);
			// console.log("Networks", SimpleStorageContract.networks);
			const deployedNetwork = SimpleStorageContract.networks[networkId];
			const instance = new web3.eth.Contract(SimpleStorageContract.abi, deployedNetwork && deployedNetwork.address);

			// Set the state, and then proceed with reading the contract data
			this.setState({ web3, accounts, contract: instance }, this.contractReadData);
		}
		catch (error) {
			alert(`Failed to load web3, accounts, or contract. Check console for details.`);
			console.error(error);
		}
	};





	// Call readData on the SimpleStorage contract and update state
	contractReadData = async () => {
		// // Get the value from the contract to prove it worked.
		const response = await this.state.contract.methods.readData().call();

		// // Update state with the result.
		this.setState({ storageValue: response });
	};

	// Call updateData on the SimpleStorage contract and update state
	contractUpdateData = async () => {
		const { accounts, contract, formVal } = this.state;
		await contract.methods.updateData(formVal).send({ from: accounts[0] });

		// Get new data
		const response = await contract.methods.readData().call();
		this.setState({ storageValue: response });
	}


// UI 

	handleUpdateButton = async () => {
		this.contractUpdateData();
	}


	updateFormVal = (e) => {
		this.setState({ formVal: e.target.value})
	}




	render() {
		const pageName = "Simple Storage Contract";

		if (!this.state.web3) return <div>Loading...</div>;
		else return (
			<div className="App">
				<PageHeader />
				<Breadcrumbs pages={ [{ name: 'Contracts', href: '/'}, { name: pageName, href: '#'} ]}/>
				<ScreenHeader title={pageName} description="The first most basic bare-bones implementation of a getter/setter."/>

				<div className="py-8">

					<div className="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-3xl">
						<div className="md:flex">
							<div className="md:flex-shrink-0">
								<img className="h-48 w-full object-cover md:h-full md:w-72" src="https://images.unsplash.com/photo-1553178227-fae128903f42?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt="" />
							</div>
							<div className="p-4 flex-grow">
								<p className="block mt-1 text-lg leading-tight font-medium text-black">SimpleStorage Contract</p>
								<p className="mt-4 text-gray-500">The current stored value is: <strong>{this.state.storageValue}</strong></p>


								<div className="py-5 flex w-full mx-auto space-x-3">
									<div className="flex-1 relative border border-gray-300 rounded-md px-3 py-2 shadow-sm focus-within:ring-0 focus-within:ring-indigo-600 focus-within:border-indigo-600">
										<label htmlFor="name" className="absolute -top-2 left-2 -mt-px inline-block px-1 bg-white text-xs font-medium text-gray-900">Value</label>
										<input onChange={this.updateFormVal} type="text" name="name" id="name" className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 outline-none sm:text-sm" placeholder="E.g. 22"/>
									</div>
									<button onClick={this.handleUpdateButton} className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-gray-700 hover:bg-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 shadow">Update Value</button>
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>
		);
	}
}

export default SimpleStorageScreen;
