import React, { Component } from "react";
import PageHeader from '../../PageHeader';
import Breadcrumbs from '../../UI/Breadcrumbs';
import ScreenHeader from '../../UI/ScreenHeader';
import SimpleTestsContract from '../../../contracts/SimpleTests.json';
import getWeb3 from "../../../lib/getWeb3";

class SimpleTestsScreen extends Component {
	state = {
		currentNumber: 0, 
		url: '', 
		web3: null,
		accounts: null,
		contract: null,
		formVal: '',
	};

	componentDidMount = async () => {
		try {
			const web3 = await getWeb3();
			const accounts = await web3.eth.getAccounts();
			const networkId = await web3.eth.net.getId();
			// console.log("Network ID", networkId);
			// const chainId = await web3.eth.getChainId();
			// console.log("Chain ID", chainId);
			// console.log("Networks", SimpleTestsContract.networks);
			const deployedNetwork = SimpleTestsContract.networks[networkId];
			const instance = new web3.eth.Contract(SimpleTestsContract.abi, deployedNetwork && deployedNetwork.address);

			// Set the state, and then proceed with reading the contract data
			this.setState({ web3, accounts, contract: instance }, this.contractGetCurrentNumber);
		}
		catch (error) {
			alert(`Failed to load web3, accounts, or contract. Check console for details.`);
			console.error(error);
		}
	};





	// Call getCurrentNumber on the SimpleStorage contract and update state
	contractGetCurrentNumber = async () => {
		// // Get the value from the contract to prove it worked.
		const response = await this.state.contract.methods.getCurrentNumber().call();

		// // Update state with the result.
		this.setState({ currentNumber: response });
	};

	// Call setCurrentNumber on the SimpleStorage contract and update state
	contractSetCurrentNumber = async () => {
		const { accounts, contract, formVal } = this.state;
		await contract.methods.setCurrentNumber(formVal).send({ from: accounts[0] });

		// Get new data
		const response = await contract.methods.getCurrentNumber().call();
		this.setState({ currentNumber: response });
	}


	// 
	contractGetUrl = async () => {
		const response = await this.state.contract.methods.getUrl().call();
		console.log(response);
		this.setState({ url: response });
	};




// UI 

	handleUpdateButton = async () => {
		this.contractSetCurrentNumber();
	}


	updateFormVal = (e) => {
		this.setState({ formVal: e.target.value})
	}


	handleGetUrlButton = async () => {
		this.contractGetUrl();
	}





	render() {
		const pageName = "Simple Tests Contract";

		if (!this.state.web3) return <div>Loading...</div>;
		else return (
			<div className="App">
				<PageHeader />
				<Breadcrumbs pages={ [{ name: 'Contracts', href: '/'}, { name: pageName, href: '#'} ]}/>
				<ScreenHeader title={pageName} description="A selection of basic tests for setting and getting variables, concatenating strings, etc"/>

				<div className="py-8">

					<div className="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-3xl">
						<div className="md:flex">
							<div className="md:flex-shrink-0">
								<img className="h-48 w-full object-cover md:h-full md:w-72" src="https://images.unsplash.com/photo-1458682625221-3a45f8a844c7?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt="" />
							</div>
							<div className="p-8 flex-grow">
								<p className="block mt-1 text-lg leading-tight font-medium text-black">SimpleTests Contract</p>
								<p className="mt-4 text-gray-500"><strong>currentNumber</strong> is: <strong>{this.state.currentNumber}</strong></p>

								<div className="py-5 flex w-full mx-auto space-x-3">
									<div className="flex-1 relative border border-gray-300 rounded-md px-3 py-2 shadow-sm focus-within:ring-0 focus-within:ring-indigo-600 focus-within:border-indigo-600">
										<label htmlFor="name" className="absolute -top-2 left-2 -mt-px inline-block px-1 bg-white text-xs font-medium text-gray-900">Value</label>
										<input onChange={this.updateFormVal} type="text" name="name" id="name" className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 outline-none sm:text-sm" placeholder="E.g. 22"/>
									</div>
									<button onClick={this.handleUpdateButton} className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-gray-700 hover:bg-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 shadow">Update Value</button>
								</div>

								<p>
									<button onClick={this.handleGetUrlButton} className="inline-flex items-center px-5 py-3 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-gray-700 hover:bg-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 shadow">Get URL</button>
									<span className="pl-4">{ this.state.url }</span>
								</p>

							</div>
						</div>
					</div>
				</div>

			</div>
		);
	}
}

export default SimpleTestsScreen;
