import React, { Component } from "react";
import PageHeader from '../../PageHeader';
import Breadcrumbs from '../../UI/Breadcrumbs';
import ScreenHeader from '../../UI/ScreenHeader';
import BasicNFT from '../../../contracts/BasicNFT.json';
import getWeb3 from "../../../lib/getWeb3";
import axios from 'axios';


class BasicNFTScreen extends Component {
	state = {
		web3: null,
		accounts: null,
		nft: null,
		tokenId: '',
		selectedTokenId: '',
		tokenInfo: null,
	};

	componentDidMount = async () => {
		try {
			const web3 = await getWeb3();
			const accounts = await web3.eth.getAccounts();
			const networkId = await web3.eth.net.getId();
			const deployedNetwork = BasicNFT.networks[networkId];
			if (deployedNetwork) {
				const nft = new web3.eth.Contract(BasicNFT.abi, deployedNetwork.address);
				this.setState({ web3, accounts, nft });
			}
		}
		catch (error) {
			alert(`Failed to load web3, accounts, or contract. Check console for details.`);
			console.error(error);
		}
	};



	showNFT = async (tokenId) => {
		const tokenURI = await this.state.nft.methods.tokenURI(tokenId).call();
		try {
			const { data } = await axios.get(tokenURI);
			this.setState({ tokenInfo: data, selectedTokenId: tokenId });
		}
		catch(e) {
			alert(`Failed to load that tokenId`);
			console.log(e);
		}
	};



	getImageUrl = (url) => {
		if (!url) return;	//Dummy placeholder image
		else if (url.startsWith("ipfs://")) return "https://ipfs.io/ipfs/" + url.substring(7);
		else return url;
	}


// UI 

	handleViewButton = async () => {
		this.showNFT(this.state.tokenId);
	}


	updateTokenId = (e) => {
		this.setState({ tokenId: e.target.value})
	}


	render() {
		const pageName = "Basic NFT";

		if (!this.state.web3) return <div>Loading...</div>;
		else return (
			<div className="App">
				<PageHeader />
				<Breadcrumbs pages={ [{ name: 'Contracts', href: '/'}, { name: pageName, href: '#'} ]}/>
				<ScreenHeader title={pageName} description="A barebones NFT with basic minting capability"/>

				<div className="py-8">
					<div className="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-3xl">
						<div className="md:flex">
							<div className="md:flex-shrink-0">
								<img className="h-48 w-full object-cover md:h-full md:w-72" src="https://images.unsplash.com/photo-1533735380053-eb8d0759b24a?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt="" />
							</div>
							<div className="p-8 flex-grow">
								<p className="block mt-1 text-lg leading-tight font-medium text-black">{pageName}</p>

								<div className="py-5 flex w-full mx-auto space-x-3">
									<div className="flex-1 relative border border-gray-300 rounded-md px-3 py-2 shadow-sm focus-within:ring-0 focus-within:ring-indigo-600 focus-within:border-indigo-600">
										<label htmlFor="name" className="absolute -top-2 left-2 -mt-px inline-block px-1 bg-white text-xs font-medium text-gray-900">Token Id</label>
										<input onChange={this.updateTokenId} type="text" name="name" id="name" className="block w-full border-0 p-0 text-gray-900 placeholder-gray-500 outline-none sm:text-sm" placeholder="E.g. 22"/>
									</div>
									<button onClick={this.handleViewButton} className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-white bg-gray-700 hover:bg-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 shadow">Get Token</button>
								</div>

							</div>
						</div>
					</div>
				</div>



				{this.state.tokenInfo != null && (
					<div className="py-8 px-8">
						<div className="mb-20">
							<div className="my-5 flex items-center justify-center gap-6">
								<div className="px-4 py-5 bg-white shadow rounded-lg overflow-hidden sm:p-6 w-6/12 lg:w-2/12 text-center">
									<div className="text-sm font-medium text-gray-500 truncate">Showing Token</div>
									<div className="mt-1 text-3xl font-semibold text-gray-900">{this.state.selectedTokenId}</div>
								</div>
							</div>

							<div className="my-5 flex items-center justify-center gap-6">
								<div className="pa bg-white shadow rounded-lg overflow-hidden w-6/12 text-center">
									<div className="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 overflow-hidden">
										<img src={this.getImageUrl(this.state.tokenInfo.image)} alt="" className="object-cover" />
										<button type="button" className="absolute inset-0 focus:outline-none"></button>
									</div>
									<p className="mt-2 mb-2 block text-sm font-large text-lg text-gray-900">{this.state.tokenInfo.name}</p>
								</div>
							</div>
						</div>
					</div>
				)}



				<div className="mb-20"></div>

			</div>
		);
	}
}

export default BasicNFTScreen;
