import React, { Component } from "react";
import ContractList from '../ContractList';
import PageHeader from '../PageHeader';

class HomeScreen extends Component {

	render() {
		return (
			<div className="App">
				<PageHeader />

				<div className="py-4">
					<ContractList />
				</div>
			</div>
		);
	}
}

export default HomeScreen;
