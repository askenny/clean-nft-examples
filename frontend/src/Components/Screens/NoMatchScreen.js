
const NoMatchScreen = ({ location }) => (
	<div className="App">
		<div className="py-6">
			<strong>Sorry, but that page wasn't found: </strong> 
			<code>{location.pathname}</code>	
		</div>
	</div>
);

export default NoMatchScreen;