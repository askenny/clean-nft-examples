import React, { Component } from "react";
// import { Link } from 'react-router-dom'
import { CashIcon } from '@heroicons/react/solid'

const pages = [
	{
		id: 1,
		name: 'Simple Storage',
		href: '/contracts/simplestorage',
		status: 'working',
		date: '10 November, 2021',
		datetime: '2021-11-10',
	},
	{
		id: 2,
		name: 'Simple Tests',
		href: '/contracts/simpletests',
		status: 'development',
		date: '10 November, 2021',
		datetime: '2021-11-10',
	},
	{
		id: 3,
		name: 'Basic NFT',
		href: '/contracts/basicnft',
		status: 'development',
		date: '11 November, 2021',
		datetime: '2021-11-11',
	},
]

function classNames(...classes) {
	return classes.filter(Boolean).join(' ')
}


const statusStyles = {
	working: 'bg-green-100 text-green-800',
	development: 'bg-yellow-100 text-yellow-800',
	broken: 'bg-red-100 text-gray-800',
}



class ContractList extends Component {

	render() {
		return (
			<div>

				<div className="sm:block">
					<div className="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
						<div className="flex flex-col mt-2">
							<div className="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
								<table className="min-w-full divide-y divide-gray-200">
									<thead>
										<tr>
											<th className="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Contract</th>
											<th className="hidden px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider md:block">Status</th>
											<th className="px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">Last Updated</th>
										</tr>
									</thead>
									<tbody className="bg-white divide-y divide-gray-200">
										{pages.map((transaction) => (
											<tr key={transaction.id} className="bg-white">
												<td className="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
													<div className="flex">
														<a href={transaction.href} className="group inline-flex space-x-2 truncate text-sm">
															<CashIcon className="flex-shrink-0 h-5 w-5 text-gray-400 group-hover:text-gray-500" aria-hidden="true"/>
															<p className="text-gray-500 truncate group-hover:text-gray-900">
																{transaction.name}
															</p>
														</a>
													</div>
												</td>
												<td className="hidden px-6 py-4 whitespace-nowrap text-sm text-gray-500 md:block">
													<span className={classNames(statusStyles[transaction.status], 'inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium capitalize')}>
														{transaction.status}
													</span>
												</td>
												<td className="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
													<time dateTime={transaction.datetime}>{transaction.date}</time>
												</td>
											</tr>
										))}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

		);
	}

}

export default ContractList;
