import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import HomeScreen from './Components/Screens/HomeScreen';
import NoMatchScreen from './Components/Screens/NoMatchScreen';
import SimpleStorageScreen from './Components/Screens/Contracts/SimpleStorageScreen';
import SimpleTestsScreen from './Components/Screens/Contracts/SimpleTestsScreen';
import BasicNFTScreen from './Components/Screens/Contracts/BasicNFTScreen';

import './App.css';

class App extends Component {

	render() {
		return (
			<div>
				<BrowserRouter>
				<Switch>
					<Route exact path='/contracts/simplestorage' component={SimpleStorageScreen} />
					<Route exact path='/contracts/simpletests' component={SimpleTestsScreen} />
					<Route exact path='/contracts/basicnft' component={BasicNFTScreen} />
					<Route exact path='/' component={HomeScreen} />
					<Route component={NoMatchScreen} />
				</Switch>
				</BrowserRouter>

			</div>
		);
	}

}

export default App;
